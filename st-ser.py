import time, serial, struct, sys
from serial import SerialException

#Automatic connect behavior
#If you don't give an argument when running the script
#(e.g.) python st-ser.py
#then the script will connect to the first available Serial port with
#the prefix "/dev/ttyACM". Otherwise you can run "python st-ser.py 11"
#and it will try to connect to "/dev/ttyACM11"
dev_prefix = "/dev/ttyACM"
dev_num = 0
dev = dev_prefix + str(dev_num)
connected = False
while not connected:
    if len(sys.argv) == 1:
        try:
            ser = serial.Serial(dev, 9600, timeout=0)
            connected = True
            print "\nConnected to Serial device: " + dev
        except SerialException:
            #print 'SerialException'
            dev_num += 1
            dev = dev_prefix + str(dev_num)
    else:
        dev_num = sys.argv[1]
        print dev_num
        dev = dev_prefix + str(dev_num)
        try:
            ser = serial.Serial(dev, 9600, timeout=0)
            connected = True
            print "\nConnected to Serial device: " + dev
        except SerialException:
            print 'SerialException'
        
#dev = '/dev/ttyACM0'
#ser = serial.Serial(dev, 9600, timeout=0)
time.sleep(1)
ser.flushInput()
ser.flushOutput()

#These four functions I found somewhere... for unpacking and packing Serial data
def lePack(n, l):
    """ Converts integer to bytes. If length after conversion
    is smaller than given as param returned value is right-filled
    with 0x00 bytes. Use Little-endian byte order."""
    return b''.join([chr((n >> ((l - i - 1) * 8)) % 256) for i in xrange(l)][::-1])

def leUnpack(byte):
    """ Converts byte string to integer. Use Little-endian byte order."""
    return sum([ord(b) << (8 * i) for i, b in enumerate(byte)])

def bePack(n, l):
    """ Converts integer to bytes. If length after conversion
    is smaller than given as param returned value is right-filled
    with 0x00 bytes. Use Big-endian byte order."""
    return b''.join([chr((n >> ((l - i - 1) * 8)) % 256) for i in xrange(l)])

def beUnpack(byte):
    """ Converts byte string to integer. Use Big-endian byte order."""
    return sum([ord(b) << (8 * i) for i, b in enumerate(byte[::-1])])


print "Entering into Serial communication loop."
#Main loop
while 1:
    #Always just run through this loop, if there is Serial data waiting... read it!
    
    time.sleep(1)
    print 'Waiting for Serial communication from Arduino.'

    if ser.inWaiting():
        inByte = struct.unpack('c', ser.read())
        #print inByte

        
        if inByte == ('\x00',):
            ''' Arduino sends over a "0" after it has established Serial
            communication, so this case should show up on start up.
            '''

            print "\nSoft Touch Menu:"
            print "(1) Demo"
            print "(2) Single pad control"
            #Add more functionality here.
            ans = int(raw_input('...')) #Get user input from command line
            ser.write(struct.pack('B', ans)) #Send over Serial as byte


        elif inByte == ('\x01',):
            ''' If you enter a "1" as the user input above, the Python script
            will send a "1" over Serial and the Arduino will confirm by 
            sending a "1" back. The Arduino will go through a for loop, 
            toggling each relay, and the Python script will print some text
            below. Demo mode can be stopped by entering any sort of text to 
            the command line and hitting Enter. The Python script will then 
            send a byte "0" to signal to stop but the Arduino will continue 
            with the demo until it is done with the loop. '''

            print "\nDemo Menu:"
            print "(1) Cycle through all pads"
            print "(2) Add another demo here"
            ans = int(raw_input('...'))
            ser.write(struct.pack('B', ans))
            time.sleep(0.1)

            need = 1
            time.sleep(1)
            while ser.inWaiting() < need:
                # print "Have", ser.inWaiting(), "bytes. Need", need, "\b."
                pass
                
            b0 = struct.unpack('c', ser.read())

            if beUnpack(b0) == 0:
                demo = 'Cycle through all pads'
            elif beUnpack(b0) == 1:
                demo = 'Add another demo here'
            else:
                demo = 'Error: else case demo'
            print '\nRunning demo:', demo
            
            ans = raw_input('Press Enter to end demo after this iteration.')
            ser.write(struct.pack('B', 0))
            

        elif inByte == ('\x02',):
            ''' Single pad control. Python script will send a "2" over
            Serial and the Arduino will confirm by sending a "2" back.
            Then the Arduino waits for two more bytes (pad, value).
            ''' 
            print "\nEnter x coordinate: (0-7)"
            x = int(raw_input('x = '))
            print "Enter y coordinate: (0-11)"
            y = int(raw_input('y = '))
            print "\nEnter 1/0 for on/off, 2 for toggle."
            value = int(raw_input('...'))
            # ser.write(struct.pack('BBB', x, y, value))
            time.sleep(1)
            ser.write(struct.pack('B', x))
            ser.write(struct.pack('B', y))
            ser.write(struct.pack('B', value))
            
            #Wait for two confirmation bytes from the Arduino
            need = 2
            time.sleep(1)
            while ser.inWaiting() < need: 
                # print "Have", ser.inWaiting(), "bytes. Need", need, "\b."
                # time.sleep(0.1)
                pass

            b1 = struct.unpack('c', ser.read())
            b2 = struct.unpack('c', ser.read())

            # print 'b1', b1
            # print 'b2', b2

            #Indicate what pad was influenced and how.
            toggle = ''
            if beUnpack(b2) == 0:
                toggle = 'off'
            elif beUnpack(b2) == 1:
                toggle = 'on'
            elif beUnpack(b2) == 2:
                toggle = 'toggling'
            else:
                toggle = 'Error: else case value'
            print '\nPad', beUnpack(b1), 'is now', toggle, '\b.'

        elif inByte == ('\x03',):
            pass
            #Add more functionality here.
            #Be sure add item (3) to the menu!
        else:
            print 'Else case of Serial Communication loop:', inByte

